import React, { useRef } from "react";
import Slider from "react-slick";
import { IMAGES } from "@/src/components/data";
import styles from './styles.module.css';

const Status = () => {


  const myRef = useRef(null);
  const handleNextslide = () => {
    myRef.current.slickNext();
  };
  const handlePrevSlide = () => {
    myRef.current.slickPrev();
  };

  const settings = {
    prevArrow: false,
    nextArrow: false,
    infinite: true,
    speed: 400,
    slidesToShow: 8,
    slidesToScroll: 1,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 1,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 8,
          initialSlide: 2,
        },
      },

    ],
  };
  return (
    <>
      <div>
        <div className={styles.mainbox}>
          <div className="w-[100%] relative">
            <div className={styles.slidecontainer}>
              <Slider ref={myRef} {...settings}>
                {IMAGES?.map((item, index) => (
                  <div key={index} className=''>
                    <img
                      className={`${styles.centerimg}`}
                      src={item.imgurl}
                    />
                    <p className={`${styles.centerimgtext} text-sm`}>Karan</p>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Status;

