import React from 'react'
import styles from './styles.module.css'
import { FaInstagram } from "react-icons/fa6";
import { GoHomeFill } from "react-icons/go";
import { IoSearch } from "react-icons/io5";
import { MdOutlineExplore } from "react-icons/md";
import { BiMoviePlay } from "react-icons/bi";
import { FaRegHeart } from "react-icons/fa";
import { RiAddBoxLine } from "react-icons/ri";
import { CgProfile } from "react-icons/cg";
import { IoReorderThreeSharp } from "react-icons/io5";
import { RiMessengerLine } from "react-icons/ri";
import Link from 'next/link';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import { IoSearchOutline } from "react-icons/io5";
import Dropdown from 'react-bootstrap/Dropdown';
import { DropdownToggle } from 'react-bootstrap';

const Sidebar = () => {
  const [show, setShow] = useState(false);
  const [isDropdownOpen, setDropdownOpen] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const toggleDropdown = () => {
    setDropdownOpen(!isDropdownOpen);
  };
  return (
    <>
      <div className='sticky top-0'>
        <div className={`${styles.customContainer} xl:w-[240px] md:w-[80px] `}>
          <div className={`${styles.customHeader} mb-4`}>
            <p className='xl:block md:hidden'>Instagram</p>
            <span className='xl:hidden md:block'><FaInstagram /></span>
          </div>
          <div className={`flex flex-col `}>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' > <span className='pe-3' ><GoHomeFill /></span> <span className='md:hidden  xl:block'>Home</span></Link>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' onClick={handleShow}> <span className='pe-3' ><IoSearch /></span> <span className='md:hidden  xl:block'>Search</span></Link>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' > <span className='pe-3' ><MdOutlineExplore /></span> <span className='md:hidden  xl:block'>Explore</span></Link>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' > <span className='pe-3' ><BiMoviePlay /></span> <span className='md:hidden  xl:block'>Reels</span></Link>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' > <span className='pe-3' ><RiMessengerLine /></span> <span className='md:hidden  xl:block'>Messages</span></Link>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' > <span className='pe-3' ><FaRegHeart /></span> <span className='md:hidden  xl:block'>Notifications</span></Link>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' > <span className='pe-3' ><RiAddBoxLine /></span> <span className='md:hidden  xl:block'>Create</span></Link>
            <Link className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='/profile' > <span className='pe-3' ><CgProfile /></span> <span className='md:hidden  xl:block'>Profile</span></Link>
            <p className='flex align-items-center text-[18px] ps-[23px] p-[10px] mx-[12px] my-[5px] w-[215px] text-zinc-50 no-underline hover:bg-[#3b4252] hover:rounded-[10px]' href='' onClick={toggleDropdown}> <span className='pe-3' ><IoReorderThreeSharp /></span> <span className='md:hidden  xl:block'>More</span></p>
          </div>
        </div >
        <div>
          <Offcanvas show={show} onHide={handleClose}>
            <Offcanvas.Header className=''>
              <div>
                <h2>Search</h2>
                <div className='relative mt-4'>
                  <span className='cursor-pointer absolute top-3 ps-3'><IoSearchOutline size='20px' color='#8E8E8E' /></span>
                  <input className='bg-zinc-800 w-[315px] h-[40px] ps-5 rounded-md focus-visible:outline-0' type="search" placeholder='Search' />
                </div>
              </div>
            </Offcanvas.Header>
            <p className='w-full border-b-[1px] border-zinc-800 mt-1'></p>
            <Offcanvas.Body className='pt-0'>
              <p>Recent</p>
              <p className='text-[#8E8E8E] h-[70%] flex justify-center align-items-center'>No recent searches.</p>
            </Offcanvas.Body>
          </Offcanvas>
        </div>
        <div>
          <Dropdown show={isDropdownOpen} onClose={() => setDropdownOpen(false)}>
            <Dropdown.Menu>
              <Dropdown.Item href="#dropdown-item-1">Dropdown Item 1</Dropdown.Item>
              <Dropdown.Item href="#dropdown-item-2">Dropdown Item 2</Dropdown.Item>
              {/* Add more Dropdown.Items as needed */}
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    </>
  )
}

export default Sidebar