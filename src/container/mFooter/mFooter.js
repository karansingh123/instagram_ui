import React from 'react'
import { GoHomeFill } from "react-icons/go";
import { MdOutlineExplore } from "react-icons/md";
import { BiMoviePlay } from "react-icons/bi";
import { RiAddBoxLine } from "react-icons/ri";
import { RiMessengerLine } from "react-icons/ri";
import { CgProfile } from "react-icons/cg";

const MFooter = () => {
  return (
    <>
      <div className='flex justify-around py-2 bg-black '>
        <GoHomeFill size='1.8em' />
        <MdOutlineExplore size='1.8em' />
        <BiMoviePlay size='1.8em' />
        <RiAddBoxLine size='1.8em' />
        <RiMessengerLine size='1.8em' />
        <CgProfile size='1.8em' />
      </div>
    </>
  )
}

export default MFooter