import { FollowList } from '@/src/components/data'
import Footer from '@/src/components/footer'
import Link from 'next/link'
import React, { useState } from 'react'
import styles from './styles.module.css'
import Example from './modal'
const FriendsSuggestion = () => {
  const [open, setOpen] = useState(false)

  const handalopenModal = () => {
    setOpen(true)
  }
  const handalcloseModal = () => {
    setOpen(false)
  }
  return (
    <>
      <div>
        <div className='flex justify-between mt-1'>
          <h6 className='text-zinc-400'>Suggested for you</h6>
          <p className='cursor-pointer text-sm hover:text-zinc-500'>See All</p>
        </div >
        <div>
          {FollowList.map((item, index) => {
            return (
              <div key={index} className='text-white flex'>
                <img onMouseEnter={() => handalopenModal()} className='rounded-full h-[50px] w-[50px]' src={item.ImgUrl} alt="profile photo" />
                <div className='m-1 ms-2 text-sm'>
                  <p className='m-0'>{item.userId}</p>
                  <p className='text-zinc-500'>{item.userName}</p>
                </div>
                <p className='text-blue-500 hover:text-white text-sm align-items-center flex justify-end ms-3 flex-grow-[1]'>{item.follow}</p>
              </div>
            )
          })}
        </div>
        <footer className={`my-3 me-[-50px]`}>
          <div className={`${styles.footerLink}`}>
            <Link href=''>About.</Link>
            <Link href=''>Help.</Link>
            <Link href=''>Press.</Link>
            <Link href=''>API.</Link>
            <Link href=''>Jobs.</Link>
            <Link href=''>Privacy.</Link>
            <Link href=''>Terms.</Link>
            <Link href=''>Locations.</Link>
            <p href=''>Language.</p>
            <Link href=''>Meta Verified.</Link>
            <p className='ps-1 mt-3'>© 2023 Instagram from Meta</p>
          </div>
        </footer>
        {/* <Example
          open={open}
          handalcloseModal={handalcloseModal}
        /> */}
      </div >
    </>
  )
}

export default FriendsSuggestion