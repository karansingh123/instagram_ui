import Link from 'next/link';
import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Row } from 'react-bootstrap';
import { RiFacebookBoxFill } from "react-icons/ri";
import styles from './styles.module.css'
import Footer from '@/src/components/footer';
const SignUp = () => {
  return (
    <>
      <div className='mt-4 bg-white'>
        <div className='d-flex justify-content-center'>
          <div className={`${styles.formComponant}`}>
            <div className={`${styles.mobileBorder} p-4`}>
              <form className='m-1' action="">
                <dir className='d-flex justify-center ms-2 mb-4'>
                  <div className={styles.bgImage}></div>
                </dir>
                <div className='text-center'>
                  <h5 className={`${styles.heading}`}>Sign up to see photos and videos from your friends.</h5>
                </div>
                <Button className='w-100 p-1 mt-2  d-flex justify-content-center text-light mb-1' variant="primary" type="submit">
                  <RiFacebookBoxFill size='22px' color='#ffff' />
                  <b>Log in with Facebook</b></Button>
                <div className='d-flex justify-content-center pt-2'>
                  <span style={{ borderTop: '1px solid black', width: '120px', marginTop: "13px" }}></span>
                  <p className='mx-2'>OR</p>
                  <span style={{ borderTop: '1px solid black', width: '120px', marginTop: "13px" }}></span>
                </div>
                <div className=" ">
                  <Form.Group className="mb-2" controlId="formBasicEmail">
                    <Form.Control type="email" placeholder="Mobile number, or email" />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>
                  <Form.Group className="mb-2" controlId="formBasicPassword">
                    <Form.Control type="text" placeholder="Full Name" />
                  </Form.Group>
                  <Form.Group className="mb-2" controlId="formBasicPassword">
                    <Form.Control type="text" placeholder="Username" />
                  </Form.Group>
                  <Form.Group className="mb-2" controlId="formBasicPassword">
                    <Form.Control type="password" placeholder="password" />
                  </Form.Group>

                  <div className='d-flex justify-content-center  text-center'>
                    <p style={{ fontSize: '13px', color: '#807A80' }} className='ps-1'>People who use our service may have uploaded your contact information to Instagram. <samp style={{ color: '#385185' }}>Learn More</samp></p>
                  </div>
                  <div className='d-flex justify-content-center  mx-2 text-center'>
                    <p style={{ fontSize: '13px', color: '#807A80' }} className='pointer'>By signing up, you agree to our <span className='text-[#385185]'>Terms , Privacy Policy</span> and <span className='text-[#385185]'>Cookies Policy</span> .</p>
                  </div>
                  <Button className='w-100 p-1 mt-2  text-light' variant="primary" type="submit"><b>Sign up</b></Button>
                </div>
              </form>
            </div>
            <div className={`${styles.mobileBorder} d-flex justify-content-center  mt-2 p-4`}>
            <Link className=' text-zinc-800 no-underline' href='/login'><p>Have an account? <b className='text-primary '>Log in</b></p></Link>
            </div>
            <div className='mt-2'>
              <p className='d-flex justify-content-center'>Get the app.</p>
              <div className='d-flex justify-content-center mt-2'>
                <div className='pe-2'>
                  <Link target="_blank" className='block' rel="stylesheet" href="https://apps.apple.com/us/app/instagram/id389801252?vt=lo" >
                    <img src="/login/Yfc020c87j0.png" alt="" width='120px' />
                  </Link>
                </div>
                <div>
                  <Link target="_blank" rel="stylesheet" href="https://play.google.com/store/apps/details?id=com.instagram.android&referrer=ig_mid%3D39066BD7-EFA9-4990-8190-FE0B0368147B%26utm_campaign%3DloginPage%26utm_content%3Dlo%26utm_source%3Dinstagramweb%26utm_medium%3Dbadge%26original_referrer%3Dhttps%3A%2F%2Fwww.google.com%2F" >
                    <img src="/login/c5Rp7Ym-Klz.png" alt="" width='120px' />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  )
}

export default SignUp