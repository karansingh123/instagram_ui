import { ReelsContent } from '@/src/components/data'
import React from 'react'
import { BsThreeDots } from "react-icons/bs";
import { FcLike } from "react-icons/fc";
import { FaRegHeart } from "react-icons/fa";
import { FaRegComment } from "react-icons/fa6";
import { BsBookmark } from "react-icons/bs";
import InstagramContext from '../../../pages/_app'
import ReactPlayer from 'react-player';
const Reels = () => {
  return (
    <>
      <div>
        {ReelsContent.map((item, index) => {
          return (
            <div key={index}  className='mb-4 border-b-1 border-zinc-400 '>
              <div className='text-white flex max-sm:px-2'>
                <img className='rounded-full h-[30px] w-[30px] cursor-pointer' src={item.ImgUrl} alt="profile photo" />
                <div className='mt-[-5px] ms-3 text-sm'>
                  <p className='m-0 cursor-pointer'>{item.userId}</p>
                  <p className='text-zinc-500'>{item.userName}</p>
                </div>
                <p className='text-blue-100 hover:text-white text-sm align-items-center flex justify-end ms-3 flex-grow-[1] cursor-pointer'>
                  {/* {item.follow} */}
                  <BsThreeDots size='25px' />
                </p>
              </div>
              <div className='h-[410px] w-[380px] border-t-[1px] border-b-[1px] md:border-[1px] border-zinc-400 '>
                <video
                 src={item.video}
                 className='h-full w-full'
                controls
                autoPlay
                muted
                playsInline
              />
              </div>
              <div className='text-white flex mt-2 border-b-[1px] border-zinc-400 max-sm:px-2'>
                <span className='cursor-pointer'><FaRegHeart size='25px' /></span>
                <div className='ms-3'>
                  <span className='cursor-pointer'><FaRegComment size='25px' /></span>
                </div>
                <p className='text-blue-100 hover:text-white text-sm align-items-center flex justify-end ms-3 flex-grow-[1] cursor-pointer'>
                  {/* {item.follow} */}
                  <span className='cursor-pointer'><BsBookmark size='23px' /></span>
                </p>
              </div>
            </div>
          )
        })}
      </div>
    </>
  )
}

export default Reels