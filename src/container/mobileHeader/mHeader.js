import React from 'react'
import { IoSearchOutline } from "react-icons/io5";
import { FaRegHeart } from "react-icons/fa";

const MHeader = () => {
  return (
    <>
      <div className='flex justify-around mt-3'>
        <h3 className='fontStyle'>Instagram</h3>
        <div className='relative'>
          <span className='cursor-pointer absolute m-1 ps-1'><IoSearchOutline size='20px' /></span>
          <input className='bg-zinc-800 w-[230px] h-[30px] ps-5 rounded-md focus-visible:outline-0' type="search" placeholder='Search' />
        </div>
        <span className='cursor-pointer'><FaRegHeart size='25px' /></span>
      </div>
    </>
  )
}

export default MHeader