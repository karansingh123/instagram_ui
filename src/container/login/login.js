import Link from 'next/link';
import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Row } from 'react-bootstrap';
import { RiFacebookBoxFill } from "react-icons/ri";
import styles from './styles.module.css'
import Footer from '@/src/components/footer';
const Login = () => {
  const [currentImage, setCurrentImage] = useState(0);
  const images = [
    '/login/screenshot1.png',
    '/login/screenshot2.png',
    '/login/screenshot3.png',
    '/login/screenshot4.png',
  ];

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentImage((prevImage) => (prevImage + 1) % images.length);
    }, 4000);

    return () => clearInterval(intervalId);
  }, []);

  return (
    <>
      <div className='mt-5'>
        <div className='flex justify-end'>
          <Link className='text-black no-underline me-5 py-2 px-3 bg-zinc-500 rounded-3' href='/home'>Home</Link>
        </div>
        <div className='d-flex justify-content-center'>
          <div className={`${styles.mobileScreen} position-relative `}>
            <img src="/login/home-phones.png" alt="" />
            <img className='position-absolute' style={{ top: "25px", left: '156px' }} src={images[currentImage]} alt={`Image ${currentImage + 1}`} />
          </div>
          <div className={`${styles.formComponant}`}>
            <div className={`${styles.mobileBorder} p-4`}>
              <form action="">
                <dir className='d-flex justify-center ms-2 mb-4'>
                  <div className={styles.bgImage}></div>
                </dir>
                <div className=" ">
                  <Form.Group className="mb-2" controlId="formBasicEmail">
                    <Form.Control type="email" placeholder="Phone number, username, or email" />
                    <Form.Text className="text-muted">
                    </Form.Text>
                  </Form.Group>

                  <Form.Group className="mb-2" controlId="formBasicPassword">
                    <Form.Control type="password" placeholder="password" />
                  </Form.Group>
                  <Button className='w-100 p-1 mt-2  text-light ' variant="primary" type="submit"><b>Log in</b></Button>
                </div>
              </form>
              <div className='d-flex justify-content-center pt-2'>
                <span style={{ borderTop: '1px solid black', width: '120px', marginTop: "13px" }}></span>
                <p className='mx-2'>OR</p>
                <span style={{ borderTop: '1px solid black', width: '120px', marginTop: "13px" }}></span>
              </div>
              <div className='d-flex justify-content-center'>
                <RiFacebookBoxFill size='22px' color='#385185' />
                <h6 style={{ color: '#385185' }} className='ps-1'>Log in with Facebook</h6>
              </div>
              <div className='d-flex justify-content-center pt-2'>
                <p style={{ color: '#385189' }} className='pointer'>Forgot password?</p>
              </div>
            </div>
            <div className={`${styles.mobileBorder} d-flex justify-content-center  mt-2 p-4`}>
              <Link className='text-zinc-800 no-underline' href='/signup'><p>Don't have an account? <b className='text-primary '>Sign up</b></p></Link>
            </div>
            <div className='mt-2'>
              <p className='d-flex justify-content-center'>Get the app.</p>
              <div className='d-flex justify-content-center mt-2'>
                <div className='pe-2'>
                  <Link target="_blank" className='block' rel="stylesheet" href="https://apps.apple.com/us/app/instagram/id389801252?vt=lo" >
                    <img src="/login/Yfc020c87j0.png" alt="" width='120px' />
                  </Link>
                </div>
                <div>
                  <Link target="_blank" rel="stylesheet" href="https://play.google.com/store/apps/details?id=com.instagram.android&referrer=ig_mid%3D39066BD7-EFA9-4990-8190-FE0B0368147B%26utm_campaign%3DloginPage%26utm_content%3Dlo%26utm_source%3Dinstagramweb%26utm_medium%3Dbadge%26original_referrer%3Dhttps%3A%2F%2Fwww.google.com%2F" >
                    <img src="/login/c5Rp7Ym-Klz.png" alt="" width='120px' />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  )
}

export default Login