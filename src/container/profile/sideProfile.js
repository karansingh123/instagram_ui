import React from 'react'
import FriendsSuggestion from '../followSuggested/friendsSuggestion'

const SideProfile = () => {
  return (
    <>
      <div className='xl:block md:hidden'>
        <div className='text-white flex mt-[40px] '>
          <img className='rounded-full h-[50px] w-[50px]' src="/status/karan.jpeg" alt="profile photo" />
          <div className='m-1 ms-2 text-sm'>
            <p className='m-0'>karansingh_8084</p>
            <p className='text-zinc-500'>Karan Singh</p>
          </div>
          <p className='text-blue-500 hover:text-white text-sm align-items-center flex justify-end ms-3 flex-grow-[1]'>Switch</p>
        </div>
        <FriendsSuggestion />
      </div>
    </>
  )
}

export default SideProfile