import React from 'react'
import Sidebar from '../sidebar/sidebar'
import { Col, Row } from 'react-bootstrap'
import Status from '../status/status'
import SideProfile from '../profile/sideProfile'
import Reels from '../reels/reels'
import MHeader from '../mobileHeader/mHeader'
import MFooter from '../mFooter/mFooter'

const MainPage = () => {
  return (
    <>
      <div className='max-sm:hidden md:block'>
        <Row>
          <Col md={1} lg={2} >
            <Sidebar />
          </Col>
          <Col md={10}>
            <Row>
              <Col md={12} lg={10} xl={8} className='ps-3 '><Status />
                <Row>
                  <Col className='flex justify-center' md={12}><Reels /></Col>
                </Row>
              </Col>
              <Col md={3} className='ps-4'><SideProfile /></Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div className='md:hidden sm-block'>
        <Row>
          <Col className='' md={12}>
            <div><MHeader /></div>
            <div className='ms-4'><Status /></div>
            <div className='mb-5'><Reels /></div>
            <div className='fixed bottom-0 left-0 right-0'><MFooter /></div>
          </Col>
        </Row>
      </div>
    </>
  )
}

export default MainPage

