import React, { useEffect, useState } from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { IoIosSettings } from "react-icons/io";
import { CiFacebook } from "react-icons/ci";
import Link from 'next/link';
import Slider from "react-slick";
import { FavStatus } from '@/src/components/data';
import { BsBookmark } from "react-icons/bs";
import { BiMoviePlay } from "react-icons/bi";
import { BsGrid3X3 } from "react-icons/bs";
 import axios from 'axios';

const ProfilePage = () => {

  const [userData, setUserData] = useState(null);
  const [posts, setPosts] = useState([]);
  const accessToken = 'IGQWRQYzJFcjZAlQXgxcXRMS1ZAnbGhYejFSUk4xbXVFbUp5NkM3OWFTRWQxTWQzSEhrRjhSZAVo4cjFWcHJnaWc5UmQwVGJmRHFtWE0wZAkJIbnZA1cGd0SEhQTTFwUWJGUi1HaUJ1UlRMa0ZASUHBZARzltb245VVpmMFkZD'; // Replace with your actual access token
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await axios.get(
          `https://graph.instagram.com/v12.0/me?fields=id,username,account_type&access_token=${accessToken}`
        );

        if (response.data.error) {
          console.error(`Error: ${response.data.error.message}`);
        } else {
          setUserData(response.data);
          fetchUserPosts();
          console.log(response.data, '544554544545544545')
        }
      } catch (error) {
        console.error(`An error occurred: ${error.message}`);
      }
    };

    const fetchUserPosts = async () => {
      try {
        const response = await axios.get(
          `https://graph.instagram.com/v12.0/me/media?fields=id,caption,media_type,media_url,thumbnail_url,permalink,reels,timestamp&access_token=${accessToken}`
        );

        if (response.data.error) {
          console.error(`Error: ${response.data.error.message}`);
        } else {
          setPosts(response.data.data);
          console.log(response.data.data, '999999999999')
        }
      } catch (error) {
        console.error(`An error occurred: ${error.message}`);
      }
    };

    fetchUserData();
  }, [accessToken]);
  const settings = {
    prevArrow: false,
    nextArrow: false,
    infinite: true,
    speed: 400,
    slidesToShow: 8,
    slidesToScroll: 1,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 1,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 8,
          initialSlide: 2,
        },
      },

    ],
  };
  return (
    <>
      <div className=''>
        <Row>
          <Col md={4} className='flex justify-center pt-2 ps-3 ' ><img className='rounded-full h-[150px]' src="/status/karan.jpeg" alt="ProfilePic" height='150px' width='150px' /></Col>
          <Col md={8}>
            <div className='ms-4'>
              <div className='flex gap-3'>
                <p className='m-0 text-xl'>karansingh_8084</p>
                <button className='rounded-1 px-3 bg-[#363636] hover:bg-[#262626]'>Edit Profile</button>
                <button className='rounded-1 px-3 bg-[#363636] hover:bg-[#262626]'>View archive</button>
                <button className='rounded-1 px-3 bg-[#363636] hover:bg-[#262626]'>Ad tools</button>
                <p className='m-0'><IoIosSettings color='white' size='2rem' /></p>
              </div>
              <div className='flex gap-4 pt-2'>
                <p>31 posts</p>
                <p>830 followers</p>
                <p>90 following</p>
              </div>
              <div>
                <p className='m-0'>Karan Singh</p>
                <p className='m-0'>Love Cricket 🇮🇳</p>
                <p className='m-0'>ठाकुर साहब😎</p>
                <p className='m-0'>Gym lover 😘😘</p>
                <p className='m-0'>Make your body strong 🦾🏋️</p>
                <span className='flex'><CiFacebook /><Link href="" className='text-white ps-1 mt-[-5px] no-underline hover:underline' >Facebook Profile</Link></span>
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col className='m-5 ps-4 pe-[30px]'>
            <Slider {...settings}>
              {FavStatus?.map((item, index) => (
                <div key={index} className=''>
                  <img
                    className={index >= FavStatus.length - 3 ? 'border-1 h-[80px] w-[80px] rounded-full p-[2px]' : 'h-[80px] w-[80px] rounded-full'}
                    src={item.ImgUrls}
                    alt={item.title}
                  />
                  <p className={`ps-3 pt-2 text-sm`}>{item.title}</p>
                </div>
              ))}
            </Slider>
          </Col>
        </Row>
        <Row><Col className='ms-5 border-b-[1px] border-zinc-900 '></Col></Row>
        <Row>
          <Col className='flex justify-center gap-5 mt-3 text-sm'>
            <Link className='text-white no-underline active:border-t-2 ' href=''> <span className='flex gap-1'><span className='mt-[5px]'><BsGrid3X3 size='11px' /></span> POSTS</span></Link>
            <Link className='text-white no-underline active:border-t-2 ' href=''> <span className='flex gap-1'><span className='mt-[5px]'><BiMoviePlay size='11px' /></span> REELS</span></Link>
            <Link className='text-white no-underline active:border-t-2 ' href=''> <span className='flex gap-1'><span className='mt-[5px]'><BsBookmark size='11px' /></span> SAVED</span></Link>
            <Link className='text-white no-underline active:border-t-2 ' href=''> <span className='flex gap-1'><span className='mt-[5px]'><svg aria-label="" className="x1lliihq x1n2onr6 x1roi4f4" fill="currentColor" height="12" role="img" viewBox="0 0 24 24" width="12"><title></title><path d="M10.201 3.797 12 1.997l1.799 1.8a1.59 1.59 0 0 0 1.124.465h5.259A1.818 1.818 0 0 1 22 6.08v14.104a1.818 1.818 0 0 1-1.818 1.818H3.818A1.818 1.818 0 0 1 2 20.184V6.08a1.818 1.818 0 0 1 1.818-1.818h5.26a1.59 1.59 0 0 0 1.123-.465Z" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></path><path d="M18.598 22.002V21.4a3.949 3.949 0 0 0-3.948-3.949H9.495A3.949 3.949 0 0 0 5.546 21.4v.603" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></path><circle cx="12.072" cy="11.075" fill="none" r="3.556" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></circle></svg></span> TAGGED</span></Link>
          </Col>
        </Row>
        <div>
          {posts.length > 0 && (
            <div>
              <Row>
                {posts?.map((post) => (
                  <Col key={post.id} className='mt-5'>
                    <iframe
                      src={post.media_url}
                      width={300}
                      height={410}
                      title="Video"
                    />

                  </Col>
                ))}
              </Row>

            </div>
          )}
        </div>
      </div>
    </>
  )
}

export default ProfilePage