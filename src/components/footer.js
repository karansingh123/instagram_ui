import Link from 'next/link'
import React from 'react'
import { Col, Row } from 'react-bootstrap'
import styles from './styles.module.css'
const Footer = () => {
  return (
    <div>
      <Row>
        <Col className={`${styles.colCenter} mt-3`}>
          <footer className={`${styles.footer} mb-5`}>
            <div className={`${styles.footerLink}`}>
              <Link href=''>Meta</Link>
              <Link href=''>About</Link>
              <Link href=''>Blog</Link>
              <p href=''>Jobs</p>
              <Link href=''>Help</Link>
              <Link href=''>API</Link>
              <p href=''>Privacy</p>
              <p href=''>Terms</p>
              <p href=''>Locations</p>
              <p href=''>Instagram Lite</p>
              <Link href=''>Threads</Link>
              <p href=''>Contact uploading and non-users</p>
              <p href=''>Meta Verified</p>
              <p href=''>English (UK) </p>
              <p>© 2023 Instagram from Meta</p>
            </div>
          </footer>
        </Col>
      </Row>
    </div>
  )
}

export default Footer