import { GoHomeFill } from "react-icons/go";
import { IoSearch } from "react-icons/io5";
import { MdOutlineExplore } from "react-icons/md";
import { BiMoviePlay } from "react-icons/bi";
import { FaRegHeart } from "react-icons/fa";
import { RiAddBoxLine } from "react-icons/ri";
import { CgProfile } from "react-icons/cg";
import { IoReorderThreeSharp } from "react-icons/io5";
import { RiMessengerLine } from "react-icons/ri";

export const navigation = [
  {
    icon: < GoHomeFill size='1.5em' />,
    title: "Home",
    // href: "/profile/orders",
    // activeIcon: trackOrderActive,
    // inActiveIcon: trackOrderIcon,
    // myOrderList: true,
  },
  {
    icon: < IoSearch size='1.5em' />,
    title: "Search",
    // href: "/profile/recently-viewed",
    // activeIcon: recentlyViewActive,
    // inActiveIcon: recentlyViewIcon,
  },
  {
    icon: < MdOutlineExplore size='1.5em' />,
    title: "Explore",
    // href: "/profile/manage-address",
    // activeIcon: locationActive,
    // inActiveIcon: manageAddressIcon,
  },
  {
    icon: < BiMoviePlay size='1.5em' />,
    title: "Reels",
    // href: "/profile/gold_mine",
    // activeIcon: goldMineIcon,
    // inActiveIcon: goldMineIcon
  },
  {
    icon: < RiMessengerLine size='1.5em' />,
    title: "Messages",
    // href: "/profile/e-kyc",
    // activeIcon: ekycIcon,
    // inActiveIcon: ekycIcon,
  },
  {
    icon: < FaRegHeart size='1.5em' />,
    title: "Notifications",
    // href: "/profile/profile-details",
    // activeIcon: manageProfileIcon,
    // inActiveIcon: profileInactiveIcon,
  },
  {
    icon: < RiAddBoxLine size='1.5em' />,
    title: "Create",
    // href: "/profile/wallet",
    // activeIcon: walletActive,
    // inActiveIcon: walletIcon,
  },
  {
    icon: < CgProfile size='1.5em' />,
    title: "Profile",
    // href: "/profile/coupons",
    // activeIcon: activeCoupon,
    // inActiveIcon: couponIcon,
  },
  {
    icon: < IoReorderThreeSharp size='1.5em' />,
    title: "More",
    // href: "/cancellation-return-policy",
    // activeIcon: "/img/policyIcon.svg",
    // inActiveIcon: "/img/profile/policiesIcon.svg",
  }
];

export const IMAGES = [
  {
    _id: "1",
    title: "Rings",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "2",
    title: "Earrings",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "3",
    title: "Bracelets",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "4",
    title: "Pendants",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "5",
    title: "Bangels",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "6",
    title: "Gold Coins",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "7",
    title: "Mangalsutra",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "8",
    title: "Necklaces",
    imgurl: "status/karan.jpeg",
  },
  {
    _id: "9",
    title: "Nosepin",
    imgurl: "status/karan.jpeg",
  },
];


export const FollowList = [
  {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow'
  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow'
  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow'
  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow'
  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow'
  },
]

export const ReelsContent = [
  {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow',
    video: "/reels/1.mp4",
  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow',
    video: "/reels/2.mp4"

  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow',
    video: "/reels/3.mp4"

  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow',
    video: "/reels/4.mp4"

  }, {
    ImgUrl: "status/karan.jpeg",
    userName: 'Suraj',
    userId: 'suraj123',
    follow: 'Follow',
    video: "/reels/5.mp4"
  },
]

export const FavStatus = [
  {
    ImgUrls: '/profile/1.jpg',
    title: 'like'
  },
  {
    ImgUrls: '/profile/2.jpg',
    title: 'Enjoying'
  },
  {
    ImgUrls: '/profile/3.jpg',
    title: 'Yaar mere'
  }, {
    ImgUrls: '/profile/4.jpg',
    title: 'Gym'
  }, {
    ImgUrls: '/profile/5.jpg',
    title: 'Chauhan Sah...'
  }, {
    ImgUrls: '/profile/6.jpg',
    title: 'Favorites'
  }, {
    ImgUrls: '/profile/7.jpg',
    title: 'Birthday'
  }, {
    ImgUrls: '/profile/8.jpg',
    title: '8084'
  }, {
    ImgUrls: '/profile/9.jpg',
    title: 'Army'
  }, {
    ImgUrls: '/profile/10.png',
    title: 'New'
  },
]