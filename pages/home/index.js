import MainPage from '@/src/container/main/mainPage'
import React from 'react'

const index = () => {
  return (
    <div className='bg-black text-white'><MainPage /></div>
  )
}

export default index