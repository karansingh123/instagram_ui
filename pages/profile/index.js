import ProfilePage from '@/src/container/profilePage/profilePage'
import Sidebar from '@/src/container/sidebar/sidebar'
import React from 'react'
import { Col, Row } from 'react-bootstrap'

const index = () => {
  return (
    <div className='bg-black text-white'> <Row>
      <Col md={1} lg={2} >
        <Sidebar />
      </Col>
      <Col md={10} lg={9} className='m-5'><ProfilePage /></Col>
    </Row></div>
  )
}

export default index